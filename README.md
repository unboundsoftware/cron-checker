# cron-checker
[![Build Status](https://gitlab.com/unboundsoftware/cron-checker/badges/master/pipeline.svg)](https://gitlab.com/unboundsoftware/cron-checker/commits/master)
[![codecov](https://codecov.io/gl/unboundsoftware/cron-checker/branch/master/graph/badge.svg)](https://codecov.io/gl/unboundsoftware/cron-checker)

A small container which periodically (every 60s) checks for a cronjobs which are not running according to schedule and sends a notification to Slack.

[Example deployment-file](example/deploy.yaml)
